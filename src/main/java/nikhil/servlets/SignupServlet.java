package nikhil.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import nikhil.DocumentManagement.App;
import nikhil.service.Utility;

public class SignupServlet extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JSONObject request = Utility.getJsonObject(req);
		Map<String, String> userInfo = Utility.getUserInfo();
		JSONObject response = new JSONObject();
		String userDirectoryPath = System.getProperty("user.dir") + "\\userDocuments";
		
		String userName = request.has("userName") ? request.getString("userName").trim() : null;
		String password = request.has("password") ? request.getString("password").trim() : null;
		String confirmPassword = request.has("confirmPassword") ? request.getString("confirmPassword").trim() : null;
		
		if(userName == null || password == null || confirmPassword == null || userName.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
			resp.getWriter().print(response.toString());
			return;
		}
		
		if(userInfo.containsKey(userName)) {
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.put("error", true).put("code", "ALREADY_EXIST").put("message", "Username already exist.");
		}else {
			if(password.equals(confirmPassword)) {
				String userDirectoryName = userName + "_" + Utility.getMd5(password);
				Utility.createDirectory(userDirectoryPath, userDirectoryName);
				String token = Utility.getMd5(userName);
				App.loginUsers.put(token, new JSONObject().put("username", userName).put("directoryName", userDirectoryName));
				
				resp.setStatus(HttpServletResponse.SC_OK);
				response.put("error", false).put("code", "SUCCESS").put("message", "successfully login.").put("result", new JSONObject().put("token", token).put("userName", userName));
			}else {
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.put("error", true).put("code", "PASSWORD_NOT_MATCH").put("message", "Password dosen't match.");
			}
		}
		resp.getWriter().print(response.toString());
	}
}
