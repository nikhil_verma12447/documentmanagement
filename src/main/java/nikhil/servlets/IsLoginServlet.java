package nikhil.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import nikhil.DocumentManagement.App;
import nikhil.service.Utility;

public class IsLoginServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JSONObject request = Utility.getJsonObject(req);
		JSONObject response = new JSONObject();
		
		String token = request.has("token") ? request.getString("token").trim() : null;
		
		if(token == null || token.isEmpty()) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
		}else {
			JSONObject result = new JSONObject();
			if(App.loginUsers.containsKey(token)) {
				result.put("isLogin", true);
				result.put("username", App.loginUsers.get(token).getString("username"));
				resp.setStatus(HttpServletResponse.SC_OK);
				response.put("error", false).put("code", "USER_LOGIN").put("message", "User is login.").put("result", result);
			}else {
				result.put("isLogin", false);
				resp.setStatus(HttpServletResponse.SC_OK);
				response.put("error", false).put("code", "USER_LOGOUT").put("message", "User is logout.").put("result", result);
			}
		}
		resp.getWriter().print(response.toString());
	}
}
