package nikhil.servlets;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONObject;

import nikhil.DocumentManagement.App;
import nikhil.service.Utility;

public class UploadFilesServlet extends HttpServlet{
	private File file ;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String token = null;
		String filePath = null;
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		JSONObject response = new JSONObject();
		try { 
			List fileItems = upload.parseRequest(req);

			Iterator i = fileItems.iterator();

			while ( i.hasNext () ) {
				FileItem fi = (FileItem)i.next();
				if ( !fi.isFormField () ) {
					// Get the uploaded file parameters
					String fileName = fi.getName();

					// Write the file
					if( fileName.lastIndexOf("\\") >= 0 ) {
						file = new File( filePath + "\\" + fileName.substring( fileName.lastIndexOf("\\"))) ;
					} else {
						file = new File( filePath + "\\" + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
					}
					fi.write( file ) ;
				}else {
					switch (fi.getFieldName()) {
					case "token":
						token = fi.getString().trim();
						break;
					case "path":
						filePath = System.getProperty("user.dir") + "\\userDocuments\\" + App.loginUsers.get(token).getString("directoryName") + "\\" + fi.getString().trim();
						break;
					}
				}
			}
		} catch(Exception ex) {
			System.out.println(ex);
		}
		
		response.put("error", false).put("code", "SUCCESS").put("message", "Files are successfully upload.");
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.getWriter().print(response);
	}
}
