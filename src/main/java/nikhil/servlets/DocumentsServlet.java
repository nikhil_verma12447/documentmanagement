package nikhil.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import nikhil.DocumentManagement.App;
import nikhil.service.Utility;

public class DocumentsServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JSONObject request = Utility.getJsonObject(req);
		JSONObject response = new JSONObject();

		String token = request.has("token") ? request.getString("token").trim() : null;
		String path= request.has("path") ? request.getString("path").trim() : null;
		
		if(token == null || token.isEmpty()) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
		}else {
			String userDir = System.getProperty("user.dir") + "\\userDocuments\\" + App.loginUsers.get(token).getString("directoryName");
			JSONObject result = new JSONObject();
			if(App.loginUsers.containsKey(token)) {
				result.put("path", path);
				result.put("documents", Utility.getAllFilesAndFolders(userDir + path));
				result.put("storage", Utility.getFolderSize(new File(userDir)));
				resp.setStatus(HttpServletResponse.SC_OK);
				response.put("error", false).put("code", "SUCCESS").put("message", "Successfully get user documents.").put("result", result);
			}else {
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.put("error", true).put("code", "INVALID_USER").put("message", "User does not exist.");
			}
		}
		resp.getWriter().print(response.toString());
	}
}
