package nikhil.servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import nikhil.DocumentManagement.App;
import nikhil.service.Utility;

public class OperationsServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JSONObject request = Utility.getJsonObject(req);
		JSONObject response = new JSONObject();
		String action = req.getQueryString().split("=")[1];

		String token = request.has("token") ? request.getString("token").trim() : null;

		if(token == null || token.isEmpty()) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
			resp.getWriter().print(response.toString());
			return;
		}

		if(App.loginUsers.containsKey(token)) {
			String path = request.has("path") ? request.getString("path").trim() : null;
			if(path == null) {
				resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
				resp.getWriter().print(response.toString());
				return;
			}

			String userDir = System.getProperty("user.dir") + "\\userDocuments\\" + App.loginUsers.get(token).getString("directoryName");

			switch (action.toUpperCase()) {
			case "CREATE":
				String folderName = request.has("folderName") ? request.getString("folderName").trim() : null;
				if(folderName == null || folderName.isEmpty()) {
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
					resp.getWriter().print(response.toString());
					return;
				}
				
				if(createFolder(folderName, userDir + path)) {
					resp.setStatus(HttpServletResponse.SC_OK);
					response.put("error", false).put("code", "SUCCESS").put("message", "Folder successfully created.");
				}else {
					resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
					response.put("error", true).put("code", "FOLDER_ERROR").put("message", "Folder is not created.");
				}
				break;	
			case "COPY":
				JSONArray selectedFiles = request.has("selectedDocuments") ? new JSONArray(request.getString("selectedDocuments")) : null;
				boolean isContain = false;
				boolean isCopySuccessFully = false;
				long freeSpace = App.MAX_SIZE_ALLOW - Utility.getFolderSize(new File(userDir));
				long selectedFileSize = 0;
				
				for(int i=0; i<selectedFiles.length(); i++) {
					if(selectedFiles.getString(i).equals(path)) {
						isContain = true;
					}
					selectedFileSize += Utility.getFolderSize(new File(userDir + selectedFiles.getString(i)));
				}
				
				if(freeSpace >= selectedFileSize) {
					if(isContain) {
						resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
						response.put("error", true).put("code", "COPY_ERROR").put("message", "Can not copy in the same folder.");
					}else {
						for(int i=0; i<selectedFiles.length(); i++) {
							isCopySuccessFully = copyFileAndFolder(userDir + selectedFiles.getString(i), userDir + path);
						}
						
						if(isCopySuccessFully) {
							resp.setStatus(HttpServletResponse.SC_OK);
							response.put("error", false).put("code", "SUCCESS").put("message", "Successfully copyed.");
						}else {
							resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
							response.put("error", true).put("code", "COPY_ERROR").put("message", "Copy failed.");
						}
					}
				}else {
					resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
					response.put("error", true).put("code", "SPACE_ERROR").put("message", "Not enough space.");
				}
				break;
			case "MOVE":
				JSONArray selectedFilesAndFolders = request.has("selectedDocuments") ? new JSONArray(request.getString("selectedDocuments")) : null;
				boolean isFolderContain = false;
				boolean isMoveSuccessFully = false;
				
				for(int i=0; i<selectedFilesAndFolders.length(); i++) {
					if(selectedFilesAndFolders.getString(i).equals(path)) {
						isFolderContain = true;
					}
				}
				if(isFolderContain) {
					resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
					response.put("error", true).put("code", "MOVE_ERROR").put("message", "Can not move in the same folder.");
				}else {
					for(int i=0; i<selectedFilesAndFolders.length(); i++) {
						isMoveSuccessFully = moveFileAndFolder(userDir + selectedFilesAndFolders.getString(i), userDir + path);
					}
					
					if(isMoveSuccessFully) {
						resp.setStatus(HttpServletResponse.SC_OK);
						response.put("error", false).put("code", "SUCCESS").put("message", "Successfully moved.");
					}else {
						resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
						response.put("error", true).put("code", "MOVE_ERROR").put("message", "Move failed.");
					}
				}
				break;
			case "DELETE":
				boolean successfullyDelete = true;
				JSONArray selectedDocuments = request.has("selectedDocuments") ? new JSONArray(request.getString("selectedDocuments")) : null;
				if(selectedDocuments == null || selectedDocuments.length() == 0){
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
					resp.getWriter().print(response.toString());
					return;
				}
				
				for(int i=0; i<selectedDocuments.length(); i++) {
					if(deleteFileOrFolders(new File(userDir + selectedDocuments.getString(i)))) {
						successfullyDelete = true;
					}else {
						successfullyDelete = false;
					}
				}
				
				if(successfullyDelete) {
					resp.setStatus(HttpServletResponse.SC_OK);
					response.put("error", false).put("code", "SUCCESS").put("message", "Successfully deleted.");
				}else {
					resp.setStatus(HttpServletResponse.SC_OK);
					response.put("error", true).put("code", "DELETE_ERROR").put("message", "Files are not delete.");
				}
				break;
			case "RENAME":
				String oldName = request.has("oldName") ? request.getString("oldName") : null;
				String newName = request.has("newName") ? request.getString("newName") : null;
				
				if(oldName == null || newName == null || oldName.isEmpty() || newName.isEmpty()) {
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
					resp.getWriter().print(response.toString());
					return;
				}
				String oldFilePath = userDir + path + "\\" + oldName;
				String newFilePath = userDir + path + "\\" + newName;
				
				if(renameFileAndFolder(oldFilePath, newFilePath)) {
					resp.setStatus(HttpServletResponse.SC_OK);
					response.put("error", false).put("code", "SUCCESS").put("message", "Successfully renamed.");
				}else {
					resp.setStatus(HttpServletResponse.SC_OK);
					response.put("error", true).put("code", "RENAME_ERROR").put("message", "Rename failed.");
				}
				break;
			}
		}else {
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.put("error", true).put("code", "INVALID_USER").put("message", "User is not valid.");
		}
		resp.getWriter().print(response.toString());
	}

	private boolean createFolder(String folderName, String currentUserDir) {
		File folder = new File(currentUserDir + "\\" + folderName);
		return folder.mkdir();
	}

	private boolean deleteFileOrFolders(File file) {
		if(file.isDirectory()) {
			for (File f : file.listFiles()) {
				if(f.isDirectory()) {
					deleteFileOrFolders(f);
				}else {
					f.delete();
				}
			}
		}
		
		return file.delete();
	}
	
	private boolean renameFileAndFolder(String oldPath, String newPath) {
		File oldFile = new File(oldPath);
		File newFile = new File(newPath);
		
		return oldFile.renameTo(newFile);
	}
	
	private boolean copyFileAndFolder(String sourcePath, String targetPath) {
		String [] temp = sourcePath.split("\\\\");
		targetPath = targetPath + "\\" + temp[temp.length -1];
		
		File srcFile = new File(sourcePath);
		File targetFile = new File(targetPath);
		
		boolean isCopy = false;
		try {
			if(srcFile.isDirectory()) {
				FileUtils.copyDirectory(srcFile, targetFile);
				
			}else {
				FileUtils.copyFile(srcFile, targetFile);
			}
			isCopy = true;
		} catch (Exception e) { 
		}
		
		return isCopy;
	}
	
	private boolean moveFileAndFolder(String sourcePath, String targetPath) {
		String [] temp = sourcePath.split("\\\\");
		targetPath = targetPath + "\\" + temp[temp.length -1];
		
		File srcFile = new File(sourcePath);
		File targetFile = new File(targetPath);
		
		boolean isMove = false;
		try {
			if(srcFile.isDirectory()) {
				FileUtils.moveDirectory(srcFile, targetFile);
				
			}else {
				FileUtils.moveFile(srcFile, targetFile);
			}
			isMove = true;
		} catch (Exception e) { 
		}
		
		return isMove;
	}
}
