package nikhil.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import nikhil.DocumentManagement.App;
import nikhil.service.Utility;

public class LoginServlet extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JSONObject request = Utility.getJsonObject(req);
		Map<String, String> userInfo = Utility.getUserInfo();
		JSONObject response = new JSONObject();
		
		String userName = request.has("userName") ? request.getString("userName").trim() : null;
		String password = request.has("password") ? request.getString("password").trim() : null;
		
		if(userName == null || userName.isEmpty()) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.put("error", true).put("code", "USERNAME_REQUIRED").put("message", "UserName required.");
			resp.getWriter().print(response.toString());
			return;
		}
		
		if(password == null || password.isEmpty()) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.put("error", true).put("code", "PASSWORD_REQUIRED").put("message", "password required.");
			resp.getWriter().print(response.toString());
			return;
		}else {
			password = Utility.getMd5(password);
		}
		
		if(userInfo.containsKey(userName)) {
			if(userInfo.get(userName).equals(password)) {
				String token = Utility.getMd5(userName);
				App.loginUsers.put(token, new JSONObject().put("username", userName).put("directoryName", userName + "_" + password));
				
				resp.setStatus(HttpServletResponse.SC_OK);
				response.put("error", false).put("code", "SUCCESS").put("message", "successfully login.").put("result", new JSONObject().put("token", token).put("userName", userName));
			}else {
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.put("error", true).put("code", "INCORRECT_PASSWORD").put("message", "Password doen't match.");
			}
		}else {
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.put("error", true).put("code", "INVALID_USER").put("message", "User does not exist.");
		}
		resp.getWriter().print(response.toString());
	}
}
