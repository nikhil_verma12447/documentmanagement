package nikhil.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import nikhil.DocumentManagement.App;
import nikhil.service.Utility;

public class LogoutServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JSONObject request = Utility.getJsonObject(req);
		JSONObject response = new JSONObject();
		
		String token = request.has("token") ? request.getString("token").trim() : null;
		
		if(token == null || token.isEmpty()) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.put("error", true).put("code", "INVALID_REQUEST").put("message", "Request is not valid.");
		}else {
			if(App.loginUsers.containsKey(token)){
				App.loginUsers.remove(token);
				resp.setStatus(HttpServletResponse.SC_OK);
				response.put("error", false).put("code", "SUCCESS").put("message", "Successfully logout.");	
			}else {
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.put("error", true).put("code", "INVALID_TOKEN").put("message", "Token is not valid.");
			}
		}
		resp.getWriter().print(response.toString());	
	}
}
