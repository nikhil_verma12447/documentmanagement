package nikhil.DocumentManagement;

import java.io.File;
import java.util.EnumSet;
import java.util.HashMap;

import javax.servlet.DispatcherType;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.json.JSONObject;

import nikhil.servlets.DocumentsServlet;
import nikhil.servlets.IsLoginServlet;
import nikhil.servlets.LoginServlet;
import nikhil.servlets.LogoutServlet;
import nikhil.servlets.OperationsServlet;
import nikhil.servlets.SignupServlet;
import nikhil.servlets.TestServlet;
import nikhil.servlets.UploadFilesServlet;

public class App 
{
	public static HashMap<String, JSONObject> loginUsers = new HashMap<String, JSONObject>();
	public static final long MAX_SIZE_ALLOW = 1000000000;   //1 GB
	public static final String userDirectoryName = "userDocuments";
	
    public static void main( String[] args )
    {
    	
    	boolean isUserDirectoryExist = false;
        Server server = new Server(8080);
        File folder = new File(System.getProperty("user.dir"));
        for (File file : folder.listFiles()) {
			if(file.getName().equals(App.userDirectoryName)) {
				isUserDirectoryExist = true;
			}else {
				isUserDirectoryExist = false;
			}
		}
        if(!isUserDirectoryExist) {
        	new File(System.getProperty("user.dir") + "\\" + App.userDirectoryName).mkdir();
        }
        
        ServletContextHandler apiHandler = new ServletContextHandler();
        apiHandler.addServlet(TestServlet.class, "/test");
        apiHandler.addServlet(IsLoginServlet.class, "/isLogin");
        apiHandler.addServlet(LoginServlet.class, "/login");
        apiHandler.addServlet(SignupServlet.class, "/signup");
        apiHandler.addServlet(LogoutServlet.class, "/logout");
        apiHandler.addServlet(DocumentsServlet.class, "/documents");
        apiHandler.addServlet(UploadFilesServlet.class, "/upload");
        apiHandler.addServlet(OperationsServlet.class, "/action");
        
        FilterHolder cors = new FilterHolder(CrossOriginFilter.class);
		cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_METHODS_HEADER, "OPTIONS,GET,POST,PUT,DELETE,HEAD");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,POST,PUT,DELETE,HEAD");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "*");
		cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, "false");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_EXPOSE_HEADERS_HEADER, "true");
		cors.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM, "X-AUTH-TOKEN");
		
		apiHandler.addFilter(cors, "/*", EnumSet.allOf(DispatcherType.class));
        HandlerCollection handler = new HandlerCollection();
        handler.setHandlers(new Handler[] {apiHandler});
        
        server.setHandler(handler);
        try {
			server.start();
			server.join();
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
}
