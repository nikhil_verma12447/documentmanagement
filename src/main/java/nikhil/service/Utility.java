package nikhil.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

public class Utility {

	public static Map<String, String> getUserInfo() {
		Map<String, String> userNameAndPasswordMap = new HashMap<String, String>();
		String userDocumentsPath = System.getProperty("user.dir") + "\\userDocuments";
		File userDocumentsFolder = new File(userDocumentsPath);
		for (File file : userDocumentsFolder.listFiles()) {
			String[] userInfo = file.getName().split("_");
			userNameAndPasswordMap.put(userInfo[0], userInfo[1]);
		}
		
		return userNameAndPasswordMap;
	}
	
	public static JSONArray getAllFilesAndFolders(String path) {
		JSONArray arrayFilesAndFolders = new JSONArray();
		File userDocumentsFolder = new File(path);
		for (File file : userDocumentsFolder.listFiles()) {
			JSONObject fileDetails = new JSONObject();
			fileDetails.put("name", file.getName());
			fileDetails.put("isDirctory", file.isDirectory());
			arrayFilesAndFolders.put(fileDetails);
		}
		return arrayFilesAndFolders;
	}

	public static void createDirectory(String path, String directoryName) {
		try {
			String command = "cd " + path + " && mkdir " + directoryName;

			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
			builder.redirectErrorStream(true);
			Process p = builder.start();
			BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			String output = "";
			while (true) {
				line = r.readLine();
				if (line == null) { break; }
				output = output + " " + line;
			}
			System.out.println(output);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static JSONObject getJsonObject(HttpServletRequest req) {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) { /*report an error*/ }
//		System.out.println(jb.toString());
		return jb.toString().isEmpty() ? new JSONObject() : new JSONObject(jb.toString());
	}

	public static String getMd5(String input) { 
		try { 
			MessageDigest md = MessageDigest.getInstance("MD5"); 
			byte[] messageDigest = md.digest(input.getBytes()); 

			BigInteger no = new BigInteger(1, messageDigest);  
			String hashtext = no.toString(16); 
			while (hashtext.length() < 32) { 
				hashtext = "0" + hashtext; 
			} 
			return hashtext; 
		}catch (NoSuchAlgorithmException e) { 
			throw new RuntimeException(e); 
		} 
	}
	
	public static long getFolderSize(File folder) {
	    long length = 0;
	    if(folder.isDirectory()) {
	    	File [] files = folder.listFiles();
		    int count = files.length;
		    for (int i = 0; i < count; i++) {
		        if (files[i].isFile()) {
		            length += files[i].length();
		        }
		        else {
		            length += getFolderSize(files[i]);
		        }
		    }
	    }else {
	    	length += folder.length();
	    }
	    return length;
	}
}
